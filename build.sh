# Install packages
sudo apt-get install -y mpic++

export PYENV_ROOT=`pwd`/pyenv
export PATH=$PYENV_ROOT/bin:$PATH
eval "$(pyenv init -)"

pyenv install 2.7.6 -f
PYTHON_CONFIGURE_OPTS="--enable-shared" pyenv install 2.7.6 -f
pyenv local 2.7.6
export CPATH=$CPATH:`python-config --prefix`/include
export LIBRARY_PATH=$LIBRARY_PATH:`python-config --prefix`/lib

# Gather SST

# Unpack
if [ ! -f sst-5.0.1 ]; then
  tar -xvf sst-5.0.1.tar.gz
fi

# Create installation path
mkdir local
mkdir local/lib
mkdir local/packages

# Get OpenMPI 1.8

# Determine install path for OpenMPI
export MPIHOME=`pwd`/local/packages/OpenMPI-1.8

# Unpack
if [ ! -f openmpi-1.8 ]; then
  tar -xvf openmpi-1.8.tar.gz

  # Build OpenMPI
  cd openmpi-1.8
  ./configure --prefix=$MPIHOME
  make all
  make install
  cd ..
fi

# Update PATH
export PATH=$MPIHOME/bin:$PATH
export MPICC=mpicc
export MPICXX=mpicxx

# Get Boost 1.56

# Determine install path for Boost
export BOOST_HOME=`pwd`/local/packages/boost-1.56

# Unpack
if [ ! -f boost_1_56_0 ]; then
  tar -xvf boost_1_56_0.tar.gz

  # Build Boost
  cd boost_1_56_0
  ./bootstrap.sh --prefix=$BOOST_HOME
  ./b2 install
  cd ..
fi

# Determine install path for SST
export SST_HOME=`pwd`/local/sst-5.0

# Build SST
cd sst-5.0.1
pyenv local 2.7.6
./configure --prefix=$SST_HOME --with-boost=$BOOST_HOME
make all
make install
cd ..
