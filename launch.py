import os
import subprocess
import json

from occam import Occam

# Gather paths
scripts_path    = os.path.dirname(__file__)
binary_path     = os.path.join(scripts_path, "local", "sst-5.0", "bin")
binary          = os.path.join(binary_path, "sst")
sst_info_binary = os.path.join(binary_path, "sstinfo")
job_path        = os.getcwd()

object = Occam.load()

inputs = object.inputs()
input_path = os.path.join(scripts_path, "sst-5.0.1", "sst", "elements", "simpleElementExample", "tests", "test_simpleMessageGeneratorComponent.py")

if len(inputs) > 0:
  files = inputs[0].files()

  if len(files) > 0:
    input_path = inputs[0].files()[0]

# Open object.json for command line options
#data = object.configuration("General")

# Generate input

# Generate input

# Form arguments

# This command will generate the sstinfo.txt directory
info_args = [sst_info_binary,
             ">"
             "sstinfo.txt"]

command = ' '.join(info_args)

os.system(command)

# Copy input configuration script
# TODO: place in a directory with all other input files
os.system("cp %s %s" % (input_path, os.path.join(job_path, "simulator.py")))

# This command will generate the graph for the given SST input file.
args = [binary,
        input_path,
        "--run-mode",
        "init",
        "-v",
        "--output-partition",
        "sst-partition-output.txt"]

# Form command line
command = ' '.join(args)

# Form command to gather results
finish_command = "python %s/compile.py" % (scripts_path)

# Tell OCCAM how to run SST
Occam.report(command, finish_command)
