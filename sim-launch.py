import os
import subprocess
import json

from occam import Occam

# Gather paths
scripts_path    = os.path.dirname(__file__)
base_path       = "/occam/9da7a968-0495-11e5-b171-001fd05bb228"
job_path        = os.getcwd()

object = Occam.load()

# Gather the SST info
dependencies = object._object.get('dependencies', [])
sst_info = dependencies[0]

# Get SST path
base_path       = os.path.join("/", "occam", "%s-%s" % (sst_info.get('id', '9da7a968-0495-11e5-b171-001fd05bb228'), sst_info.get('revision')))
binary_path     = os.path.join(base_path, "local", "sst-5.0", "bin")
binary          = os.path.join(binary_path, "sst")

input_path = os.path.join(scripts_path, "simulator.py")

# Open object.json for command line options
data = object.configuration("General")

# Generate input

# Form arguments

# This command will run the SST simulator
args = [binary,
        input_path,
        "-v"]

# Form command line
command = ' '.join(args)

# Form command to gather results
#finish_command = "ls" % (scripts_path)

# Tell OCCAM how to run the generated SST simulator
Occam.report(command)
